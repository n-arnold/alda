# 3a
# Man kann den Graphen vollständig traversieren und dabei beobachten
# ob man unter folgenden Bedingungen beim Ziel "E" ankommt:
# 1. Kein Knoten wurde zweimal durchlaufen (das sollte bereits beim
# Traversieren unterbunden werden)
# 2. Der Pfades muss 11 Kanten umfassen 
# Ein Hamilton-Weg liegt also vor, wenn der Zielknoten erreicht wurde (A), der Pfad Länge 11 hat (sofern man die Kanten zählt, bzw. den Startknoten nicht mitzählt) und kein Knoten doppelt vorkommt.
#
# Es sind sowohl Tiefensuche als auch Breitensuche denkbar. Bei der Breitensuche könnte man direkt die ersten Level mit unzureichender Länge (zu kurz um alle Knoten beinhalten zu können) überspringen. 
# Bei der Tiefensuche erreicht man E möglichst schnell und kann dann entscheiden, ob die gesuchten Eigenschaften vorliegen (ob E also mit der richtigen Länge erreicht wurde, was bei Ausschluss der Doppelungen einem Hamilton-Pfad entspricht.
# 
# Ich habe begonnen, die Tiefensuche zu implementieren, glaube aber, dass es hier schwieriger ist, den Pfad sobald man "E" zu früh erreicht hat direkt zu übergehen. In meiner Implementierung werden diese Pfade also unnötigerweise noch bis zu einer Pfadlänge von 12 weitergetestet, obwohl beim ersten frühzeitigen Auftreten des Zielknotens bereits klar ist, dass alle diese Pfade invalide sind. (Oder icih übersehe gerade, wie ich an beliebiger Stelle aus einem falschen Pfad "backtracken" kann)
#
# Manuell gefundener Weg:
# * -- N -- O -- W-- A -- Y -- I -- M -- S -- U -- R -- E
#
# 3b

from collections import deque

index_to_node_map = ["O", "Y", "I", "E",  # 0  1  2  3 
                     "N", "M", "A", "R",  # 4  5  6  7
                     "*", "S", "W", "U"]  # 8  9 10 11

G = [
        [10, 4],    [2, 6, 8],    [ 3, 5, 1],  [ 7, 2],
    # O: W, N     Y: I, A, *     I: E, M, Y      E: R, I

        [0, 6, 8],       [2, 6, 7, 9],     [1, 4, 5, 10],    [3, 5, 11],
#     N: O, A, *       M: I, A, R, S     A: Y, N, M, W     R: E, M, U 

         [1, 4],    [5, 11],       [0, 6],         [7, 9]
#      *: Y, N    S: M, U        W: O, A         U: R, S
    ]

def tief(graph, startnode, targetnode):
    visited = [False] * len(graph)
    parents = [None] * len(graph)
    path = []

    def visit(node, parent):
        if not visited[node] and not len(path) > 11:
            visited[node] = True
            parents[node] = parent
            path.append(node)
#            if len(path) > 11:
#                return None
            if node == targetnode:
                laenge = len(path)
                print("Ziel erreicht mit Pfadlänge: {}".format(laenge))
            if node == targetnode and len(path) == 11:
                raise RecursionError    # my method of getting out of here in case of success
            # sadly I did not get a feedback mail on exercise 9 where I did this as well
            for neighbour in graph[node]:
                visit(neighbour, node)
            path.pop()
            print("No hamilton way found")
    try:
        visit(startnode, startnode)
    except RecursionError:
        return direct_path


def breit(graph, startnode, targetnode):
    parents = [None] * len(graph)
    parents[startnode] = startnode
    path = []
    q = deque()
    q.append(startnode)

    while len(q) > 0:
        node = q.popleft()
        path.append(node)
        if node == targetnode and len(path) == 11:
            return path
        for neighbor in graph[node]:
            if parents[neighbor] is None:
                parents[neighbor] = node
                q.append(neighbor)

# direct_path = tief(G, 8, 3)
direct_path = breit(G, 8, 3)
print(direct_path)
print("If it says None there is (should be) no such path. (or the implementation is not right :/ )"
