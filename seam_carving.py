# "-" in filenames is not recommended or even false, might be a typo in the exercise sheet.

from pgm import read_pgm, write_pgm
from heapq import heappush, heappop
from random import randint

def create_graph(image, width, height):
    graph = [[] for i in range(len(image))]
    weights = {}
    x, y = 0, 0

    run = 0 # for debugging
    while y < height - 1: # -1 um letzte Zeile zu überspringen, da keine unteren Nachbarn möglich sind
        # eine eigene IF-Abfrage gleich zu Beginn mit "break" wäre vielleicht eleleganter
        index = x + y*width

        if x > 0:  # linker Nachbar möglich
            SW = index+width-1 # SW = south west
            graph[index].append(SW) # nächste Zeile entspricht '+ width'
            assert index+width-1 == (x + (y+1) * width) - 1
            # alternativ: SW = (x + (y+1) * width) - 1   etc...
            
            weights[(index, SW)] = abs(image[index] - image[SW])

        S = index+width
        graph[index].append(S) # vertikaler Nachbar ist immer möglich (da letzte Zeile bereits in der Schleifenbedingung behandelt wird).
        weights[(index, S)] = abs(image[index] - image[S])


        if x < (width - 1): # rechter Nachbar möglich
            SE = index+width+1
            graph[index].append(SE)
            weights[(index, SE)] = abs(image[index] - image[SE])

        x = (x + 1) % width
        if x == 0:
            y += 1
        run += 1

    # add start and end node

    last_index = len(graph) - 1 # index des letzten Bild-Pixels
    start_index = last_index + 1
    end_index = start_index + 1

    first_row = list(range(width))
    graph.append(first_row) # start node with a link to all pixels of the first row
    for pixel in first_row:
        weights[(start_index, pixel)] = 0

    graph.append([]) # end node

    for i in range(last_index - width + 1, start_index): # last row
        graph[i].append(end_index)   #oder auch: len(graph) - 1 # endknoten
                                     #assert len(graph) - 1 == l + 2
        weights[(i, end_index)] = 0

    return graph, weights


def dijkstra(graph, weights, start, target):

    parents = [None] * len(graph)
    q = []
    heappush(q, (0.0, start, start))
    while len(q) > 0:
        length, node, parent = heappop(q)
        if parents[node]:
            continue # gibt schon einen kürzeren Weg zu diesem Knoten
        parents[node] = parent
        if node == target:
            break
        for neighbor in graph[node]:
            if parents[neighbor] is None:
                new_length = length + weights[(node, neighbor)]
                heappush(q, (new_length, neighbor, node))
    if parents[target] is None: 
        return None, None # Kein Weg gefunden, da Graph unzusammenhängend
    path = [target]
    while path[-1] != start: 
        path.append(parents[path[-1]])
    path.reverse()
    return path
    # return path

def highlight_paths(image, width, height, path):
    image_copy = image[:] # copy image
    path_color = randint(0, 256) #255
    for pixel in path[1:-1]:    # slice start and end node away
#        if pixel < 79350:
        image_copy[pixel] = path_color

    return image_copy

def drop_pixels(image, width, height, path):
    image_copy = image[:]   # copy image
    path.reverse()          # delete the highest pixels first, otherwise with every
    # deletion higher indexes get off by 1 (cumulating...)
    # that bug took me a while

    for pixel in path[1:-1]:    # slice start and end node away
        if pixel < width * height:
            image_copy.pop(pixel)
        else:
            print("else case happened!")

    return image_copy


def seam_carving(image, width, height, new_width):
    new_image = image[:]
    #demo_image = image[:]
    new_graph, new_weights = create_graph(new_image, width, height)
    actual_width = width

    while actual_width > new_width:
        start = len(new_graph) - 2
        ziel = len(new_graph) - 1
        new_path = dijkstra(new_graph, new_weights, start, ziel)
        new_image = drop_pixels(new_image, actual_width, height, new_path)
        #demo_image = highlight_paths(new_image, width, height, path)

        # auch in der graph Struktur müssen die entsprechenden Pixel gelöscht werden,
        # da die dijkstra-Funktion basierend auf dieser Struktur den günstigsten Pfad ermittelt.
        # oder wir erzeugen immer wieder einen neuen graphen basierend auf dem aktuellen image (teuer!)
        # einfach nur löschen geht aber auch nicht, weil im Graphen ja die alten Indizes
        # weiterhin verwendet werden...

        actual_width -= 1
        new_graph, new_weights = create_graph(new_image, actual_width, height)


    return new_image

def seam_carving_highlight(image, width, height, new_width):
    demo_image = image[:]
    actual_width = width
    graph, weights = create_graph(demo_image, actual_width, height)
    start = len(graph) - 2
    ziel = len(graph) - 1

    while actual_width > new_width:
        new_path = dijkstra(graph, weights, start, ziel)
        demo_image = highlight_paths(new_image, width, height, path)

        # damit der gefundene Pfad beim nächsten Aufruf nicht mehr möglich ist,
        # werden hier in grober Weise die Gewichte alle auf das maximale Gewicht gesetzt:

        weights = disallow_path(weights, new_path)

        actual_width -= 1



    return new_image

def disallow_path(weights, path):
    for i in range(len(path) - 1):
        weights[(path[i], path[i + 1])] = 99999
    return weights

if __name__ == '__main__':

    width, height, image = read_pgm('coast.pgm')
    graph, weights = create_graph(image, width, height)


    start = len(graph) - 2
    ziel = len(graph) - 1

    print(graph[start])


    # b
    # Warum können diese Pixel als unwichtig betrachtet werden?
    # A: Dieser vertikale Saum trägt in sich die geringste Variation und damit
    # wahrscheinlich flächiges, ggf. hintergründige Bild Information.
    # Das menschliche Auge ist vor allen Dingen gut darin, Kontraste wahrzunehmen,
    # es werden durch diese Methode also Pixel entfernt, die gerade kaum Kontrast
    # (kaum Unterschied) haben und damit also kognitiv kaum wahrgenommen werden,
    # aber auch gemäß der Idee von "Entropie" kaum Information enthalten.
    # Es werden also eher irrelevante Pixel entfernt.

    path = dijkstra(graph, weights, start, ziel)


    new_image = drop_pixels(image, width, height, path)
    write_pgm(width - 1, height, new_image, 'coast-1.pgm')

    highlight_image = highlight_paths(image, width, height, path)
    write_pgm(width, height, highlight_image, 'coast-1-highlight.pgm')

    # c
    squared_image = seam_carving(image, width, height, height)

    write_pgm(height, height, squared_image, 'coast-square.pgm')

    # In der coast-square.pgm sieht man besonders im Himmel Artefakte,
    # weil dort der horizontale kontinuierliche Farbverlauf nicht
    # erkannt / berücksichtigt werden konnte.


    # Ich wollte alle gelöschten Seams "einfarbig" darstellen, hat sich aber nicht
    # bewährt, bzw. als wesentlich umständlichere Anpassung erwiesen

    #squared_image_highlight = seam_carving_highlight(image, width, height, height)
    #write_pgm(height, height, squared_image_highlight, 'coast-square-highlight.pgm')