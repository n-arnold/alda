import json

#import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
import matplotlib.colors
from math import floor, ceil
from heapq import heappush, heappop

from collections import defaultdict


# aus der Lösung zu ex10, 3a
def create_graph(data):
    '''Erstellt den Graphen und die benoetigten property maps'''

    # Erstelle leere Adjazenzliste
    size = len(data)
    graph = [ [] for _ in range(size) ]

    # Erstelle leere property maps
    vertex_property_map = [""]*size
    edge_property_map = {}

    # Iteration über data um die Kanten der Adjazenzliste zu erstellen
    for city, item in data.items():
        i = item["Index"]
        vertex_property_map[i] = city
        for neighbour, distance in item["Nachbarn"].items():
            j = data[neighbour]["Index"]
            graph[i].append(j)
            edge_property_map[(i, j)] = float(distance)

    return graph, vertex_property_map, edge_property_map

def find(anchors, node):
    start = node
    while node != anchors[node]:
        node = anchors[node]
    anchors[start] = node
    return node

def mst(graph, weights):
    """Kruskal"""

    # Implementierung von mst als defaultdict, so werden effektiv nur die Kanten des mst gespeichert, für alle anderen Werte (aber auch ungültige "Kanten") gibt das dict False zurück. 
    mst = defaultdict(lambda: False)

    anchors = list(range(len(graph)))
    q = []

    for edge, w in weights.items():
        heappush(q, (w, edge))

    while len(q) > 0:
        w, edge = heappop(q)
        a1 = find(anchors, edge[0])
        a2 = find(anchors, edge[1])
        if a1 != a2:
            anchors[a2] = a1
            mst[edge] = True

#            mst[edge[::-1]] = True # also add the inverted edge because this graph is not directed!
 
            # je nachdem, was man will: mit dieser Zeile kommt es später nicht mehr auf die Reihenfolge einer Edge an.
            # ggf. sollte man dies aber eher an einer anderen Stelle prüfen (also nicht nur, ob edge in MST ist, sondern
            # dabei auch, ob edge[::-1] in MST ist, da nur eine der beiden Kanten repräsentiert ist.
            # (MST speichert hier im Grunde einen gerichteten MST, der semantisch aber als ungerichtet gedeutet wird.
            # Irgendwo muss dieser Unterschied also behandelt werden - ich denke mal, dass das hier auch über die
            # Konvention von "kleiner Index zuerst" gelöst ist)
    return mst


def cluster(graph, mst_edges, weights, threshold):
    """
    returns a forest in adjazenzlisten-darstellung 
    """

    # hinweis: graph ist ungerichtet!

    # das scheint keine gute idee zu sein, weil es nur 1 list-object erstellt...
    #G = [ [] ] * len(graph) ### !! len(graph) ?? ggf. graph struktur nochmals anschauen
    # Das folgende hingegen ginge auch: graph = [ [] for _ in range(size) ]
    G = []
    for i in range(len(graph)):
        G.append([])

    for edge in mst_edges: # bei einer regulären property map müsste man hier jeweils die edges mit "True" auswählen
        if weights[edge] < threshold:
            smaller_node, bigger_node = edge # tupel entpacken
            G[smaller_node].append(bigger_node)
            G[bigger_node].append(smaller_node)

    return G


def components(graph):
    # Zusammenhangskomponenten finden mittels union find

    anchors = list(range(len(graph)))  # Zu Beginn ist jede Node eine eigene Komponente (trivial)
    labels = [None] * len(graph)

    for node in range(len(graph)):
        for neighbor in graph[node]:
            if neighbor < node:
                continue  # doppelte Kanten überspringen
            a1 = find(anchors, node)
            a2 = find(anchors, neighbor)
            if a1 < a2:  # Konvention: Entscheidung für den kleineren Anchor
                anchors[a2] = a1
            else:
                anchors[a1] = a2

    current_label = 0
    for node in range(len(graph)):
        a = find(anchors, node)
        if a == node:  # Node ist Anker und startet als kleinster Index eine neue Zush.Komponente
            labels[a] = current_label
            current_label += 1
        else:
            labels[node] = labels[a]

    return labels, (max(labels) + 1)




if __name__ == '__main__':
    with open("entfernungen.json", encoding="utf-8") as f:
        distance_dict = json.load(f)

    for i, city in enumerate(distance_dict):
        distance_dict[city]["Index"] = i

    graph, names, weights = create_graph(distance_dict)

    #1a
    mst_edges = mst(graph, weights)
    # print(mst_edges)

    # 1b
    threshold = 40
    forest = cluster(graph, mst_edges, weights, threshold)

    # 1c
    labels, count = components(forest)


    # 1d
    # Welches Verhalten zeigt die Kurve? Je höher der Schwellwert, desto weniger Cluster.
    # Bei maximaler Toleranz werden alle Städte zu einem Cluster zusammengefasst. Das ist zu erwarten.
    # Die Kurve fällt bis zu einem Schwellwert von 50 sehr steil, dann bis ca. 80 etwas weniger steil
    # und von da an ziemlich flach. Es entsteht eine Zipf-Kurve:
    # Viele Städte liegen weiter auseinander als "15", wenige Städte liegen weiter
    # auseinander als 80.

    # Kann man daraus Schwellwerte ableiten, die die Ballungsgebiete in Deutschland sicht- bar machen?
    # Ich habe eine Weile darüber nachgedacht, evtl. verstehe ich die Frage auch nicht ganz richtig,
    # oder meine Geographie-Kenntnisse sind zu beschränkt. Ich finde es schwierig, nur aus dieser Kurve Schwellwerte für die Ballungsgebiete abzuleiten.
    # Ballungsgebiete würden die Anzahl an Städten (es ballen sich viele kleine Städte auf engem Raum)
    # sowie die Größe der Städe (es ballen sich viele Menschen auf engem Raum) beinhalten.
    #
    # Man kann den Schwellwert aber durch den Plot eingrenzen:
    # Für einen Schwellwert von 40 erhält man einige schöne Cluster.
    # Für größere Werte verschmelzen Cluster miteinander, die geographisch weit auseinander liegen.


    # Welche Cluster (Städtelisten) erhalten Sie für einen geeigneten Schwellwert?
        # (siehe Ausgabe zu stdout)
    # Mir ist beim Plotten der Cluster aufgefallen, dass eine Node trotz einiger Städte
    # im direkten Umfeld, nicht zum Cluster aufgenommen wurde.
    # Ich schätze, dass es sich dabei um Stuttgart handelt.
    # Schaut man im distance-dict bei Stuttgart nach, fällt auf, dass dort nur
    # Heidelberg, Pforzheim, Ulm und Würzburg genannt sind, und nähere Städte
    # wie Karlsruhe, Reutlingen, Sindelfingen fehlen. Das führt zu dem oben genannten Fehler.
    # Evtl. verstehe ich das Datenset aber auch nicht richtig.
    # (siehe in ballungsgebiete.svg unten links das orange-farbene Cluster. Dort ist eine
    # Stadt unverbunden.)

    def get_component_count(thresh):
        """
        helper function for the use of map. for a given threshold it returns the respective component count.
        """
        forest = cluster(graph, mst_edges, weights, thresh)
        _, count = components(forest)
        return count

    weights_only = weights.values()

    # Die Idee war, die maximale Distanz aus allen weights-Einträgen herauszulesen.
    # Allerdings ist das nicht ganz der richtige Referenzwert, da uns später
    # die Distanzen von Clustern zueinander interessieren. Daher ist nun 150 hardcoded.
    #thresholds = list(range(floor(min(weights_only)), ceil(max(weights_only)), 1))
    thresholds = list(range(floor(min(weights_only)), 150, 1))

    num_of_components = list(map(get_component_count, thresholds))

    plt.plot(thresholds, num_of_components)
    plt.xlabel('thresholds')
    plt.ylabel('# of components')
    plt.savefig("threshold-vs-num_of_components.svg")
    plt.show()

    # Städteliste
    ballungs_forest = cluster(graph, mst_edges, weights, threshold)
    ballungs_labels, ballungs_count = components(ballungs_forest)
    print(ballungs_count)

    # Städteliste für gewählten Threshold (40) ausgeben
    for i in range(ballungs_count):
        print(f"\nKomponente {i}")
        for city in range(len(labels)):
            if labels[city] == i:
                print(names[city])

    def convert_longlat(str):
        """converts a long or lat string to decimals

        Ich verwende hier eine sehr naive Implentierung mit slices.
        Durch assert habe ich aber sichergestellt, dass im vorliegenden Datensatz
        alle Koordinaten dieses 5-char-format streng befolgen, daher ist es zumindest korrekt."""
        degree = str[0:2]
        direction = str[2]
        minutes = str[3:]

        # Umrechnung zwischen der String-Repräsentation und dezimaler Darstellung der Koordinaten.
        decimal = float(degree) + float(minutes) / 60
        if direction in "SW":
            decimal = decimal * (-1)
        return decimal

    geo_data = []   #   will store a node property map of long-lat tuples
    for i, v in enumerate(distance_dict.values()):
        lat, long = v["Koordinaten"]["Breite"], v["Koordinaten"]["Länge"]

        geo_data.append((convert_longlat(long), convert_longlat(lat)))
        assert len(long) == 5
        assert len(lat) == 5

    df = pd.DataFrame(geo_data, columns=["latitude", "longitude"])


    plt.scatter(df[["latitude"]], df[["longitude"]], s=10, c="b", alpha=0.5)

    # Versuch die Städte-Kreise manuell zu plotten. Stattdessen habe ich noch "scatter" verwendet.
    # büße dadurch aber Kontrolle ein.

    #for node in geo_data:
        #plt.plot([node[0], node[1]], color="green")
    #    circle1 = plt.Circle((0, 0), 0.5, color='r')


    # coloring:  - versuche eine geeignete Colormap zu finden ... ging so
    # am Ende habe ich doch mittels List comprehension soviele Farben
    # wie Komponenten vorliegen erstellt und zu rgb konvertiert.

    #norm = matplotlib.colors.Normalize(vmin=0.0,vmax=float(max(labels) + 1))
    #colors = plt.cm.nipy_spectral
    #norm = lambda x: x

    # colors = plt.cm.get_cmap(plt.cm.jet, max(labels) + 1)

    # give me enough colors for my # of components:
    N = max(labels) + 1
    hsv_color_list = [(x * 1.0 / N, 0.75, 1) for x in range(N)]
    rgb_color_list = list(map(matplotlib.colors.hsv_to_rgb, hsv_color_list))



    def normalize(value, max):
        normalized_value = (value / max)
        return normalized_value

    for edge in mst_edges:
        start, end = edge
        startx, starty = geo_data[start]
        endx, endy = geo_data[end]
        if weights[edge] < threshold:
            plt.plot([startx, endx], [starty, endy], color=rgb_color_list[labels[start]])

    # bonus:
    # idee: Heuristik: geeignete Stadt ist jeweils die Stadt eines Clusters, mit den meisten "Nachbar"-Einträgen im Forest
    # d.h.: maximiere die Anzahl der Nachbarn für jede Komponente des forest für einen bestimmten threshold
    # Besser als die Heuristik wäre es natürlich,
    # Optisch könnte man auch den Mittelpunkt aller Städte einer Komponente (bzw. deren float-Koordinaten)
    # bestimmen und die Stadt, die am nächsten an diesem Punkt liegt namentlich eintragen.
    # Ansonsten könnte man natürlich mit elaborierteren Graphen-Algorithmen die Zentralität
    # bzw. Bedeutsamkeit für die Konnextivität einer Stadt bestimmen – passender für unseren
    # Fall wäre da aber einfach die größte Stadt zu wählen (diese Daten liegen hier leider nicht vor).

    result = []
    for cluster_idx in range(max(labels) + 1):
        # gehe jede Komponente durch und finde den besten Kandidaten (hier Heuristik: meiste Nachbarn)
        # um die Komponente zu repräsentieren
        best = (None, 0)    # initial best candidate per cluster + weight
        for i in range(len(labels)):
            if labels[i] == cluster_idx:
                candidate = i
                _, current_best_num = best
                if len(forest[i]) > current_best_num:
                    best = (i, len(forest[i]))
        result.append(best)

    for index, num_of_neighbours in result:
        if index and num_of_neighbours > 0:
            coords = geo_data[index]
            plt.annotate(names[index],
                     coords, # this is the point to label
                     textcoords="offset points", # how to position the text
                     xytext=(12,-20), # distance from text to points (x,y)
                     arrowprops=dict(arrowstyle="->",
                                     connectionstyle="arc3"),
                     ha='right',
                     size=7) # horizontal alignment can be left, right or center)

    plt.savefig("ballungsgebiete.svg")
    plt.show()